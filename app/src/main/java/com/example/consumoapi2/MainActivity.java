package com.example.consumoapi2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

//import com.example.consumoapi2.Interface.Openlibra;
//import com.example.consumoapi2.Model.Posts;

import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.POST;

public class MainActivity extends AppCompatActivity {

    EditText et;
    TextView tv;
    TextView tvMin;
    TextView tvMax;
    TextView tvFL;
    TextView tvH;
    TextView tvP;
    TextView tvWind;
    TextView tvWC;
    String url = "http://api.openweathermap.org/data/2.5/weather?q={city name}&appid={API key}";
    String apikey = "fcf70842f715e501fd99014a6e08b797";
    private TextView mJsonTxtView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        et = findViewById(R.id.et);
        tv = findViewById(R.id.tv);
        tvMin = findViewById(R.id.tvmin);
        tvMax = findViewById(R.id.tvmax);
        tvFL = findViewById(R.id.tvFL);
        tvH = findViewById(R.id.tvH);
        tvP = findViewById(R.id.tvP);
        tvWind = findViewById(R.id.tvWind);
        //tvWC = findViewById(R.id.tvWC);
    }

    public void PasarOA(View view)
    {
        Intent Ac2 = new Intent(this, MainActivity2.class);
        startActivity(Ac2);
    }

    public void getweather(View v){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.openweathermap.org/data/2.5/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        weatherapi myapi = retrofit.create(weatherapi.class);
        Call<Example> examplecall = myapi.getweather(et.getText().toString().trim(),apikey);
        examplecall.enqueue(new Callback<Example>() {
            @Override
            public void onResponse(Call<Example> call, Response<Example> response) {
                if(response.code()==404){
                    Toast.makeText(MainActivity.this, "Por favor ingresa una ciudad valida", Toast.LENGTH_LONG).show();
                }
                else if(!(response.isSuccessful())){
                    Toast.makeText(MainActivity.this, response.code(), Toast.LENGTH_LONG).show();
                }
                Example mydata = response.body();
                Main main = mydata.getMain();
                Wind wind = mydata.getWind();

                Double temp = main.getTemp();
                Double tempmin = main.getTempMin();
                Double tempmax = main.getTempMax();
                Double tempFL = main.getFeelsLike();
                Integer humedad = main.getHumidity();
                Integer presion = main.getPressure();
                Double winds = wind.getSpeed();

                Integer temperature = (int) (temp-273.15);
                Integer temperaturemin = (int) (tempmin - 273.15);
                Integer temperaturemax = (int) (tempmax - 273.15);
                Integer temperatureFL = (int) (tempFL - 273.15);
                Integer humedadd = (int) (humedad);
                Integer presionn = (int) (presion);
                Double windSpeed = (double) (winds);
                tv.setText(String.valueOf(temperature)+"C");
                tvMin.setText(String.valueOf(temperaturemin)+"C");
                tvMax.setText(String.valueOf(temperaturemax)+"C");
                tvFL.setText(String.valueOf(temperatureFL)+"C");
                tvH.setText(String.valueOf(humedadd)+"hPa");
                tvP.setText(String.valueOf(presionn)+"%");
                tvWind.setText(String.valueOf(windSpeed)+"Km/h");

            }

            @Override
            public void onFailure(Call<Example> call, Throwable t) {
                Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

    }
}